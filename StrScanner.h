/*
 * StrScanner.h
 *
 *  Created on: 27 ago 2015
 *      Author: luca
 *
 *  Copyright 2015 Luca Di Stefano
 *
 *  This file is part of StrScanner.
 *
 *  StrScanner is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  StrScanner is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with StrScanner.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef STRSCANNER_H_
#define STRSCANNER_H_

#include <Arduino.h>

class StrScanner {
private:
	String _string;
	String _delim;
	String _next;
	String processToken();
public:
	StrScanner(String);
	StrScanner(String, String);

	void useDelimiter(String);

	bool hasNext();
	bool hasNextBool();
	bool hasNextByte();
	bool hasNextChar();
	bool hasNextDouble();
	bool hasNextInt();

	String next();
	bool nextBool();
	byte nextByte();
	char nextChar();
	double nextDouble();
	int nextInt();

};

#endif /* STRSCANNER_H_ */
