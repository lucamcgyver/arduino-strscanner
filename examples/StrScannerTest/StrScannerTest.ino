#include <StrScanner.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  String s = "String;300;1.5;255;true;0";

  Serial.print("Starting string: ");
  Serial.println(s);
  
  StrScanner scanner(s, ";");
  
  Serial.print("Next: ");
  Serial.println(scanner.next());
  
  Serial.print("Next int: ");
  Serial.println(scanner.nextInt());
  
  Serial.print("Next double: ");
  Serial.println(scanner.nextDouble());
  
  Serial.print("Next byte: ");
  Serial.println(scanner.nextByte());
  
  Serial.print("Next bool (literal): ");
  Serial.println(scanner.nextBool());
  
  Serial.print("Next bool (numeric): ");
  Serial.println(scanner.nextBool());
  
  Serial.print("Original string intact: ");
  Serial.println(s);
  
  Serial.println("End test");

}

void loop() {
  // put your main code here, to run repeatedly:

}
