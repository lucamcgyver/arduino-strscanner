# README #

First Release Candidate on 27/08/2015

### Changelog ###

No changes yet.

### What is StrScanner? ###

StrScanner is an Arduino library made to parse string inputs with an iterator-like syntax.

### How do I get set up? ###

* Go to the download section of the repository [https://bitbucket.org/lucamcgyver/arduino-strscanner/downloads](https://bitbucket.org/lucamcgyver/arduino-strscanner/downloads)
* Download the .zip file
* Extract it to your /Arduino/libraries directory
* Import <StrScanner.h>
* Use :)
* Check out the [Wiki](https://bitbucket.org/lucamcgyver/arduino-strscanner/wiki/Home) for help on how to use this library

### Contribution guidelines ###

* Still under construction

### Who do I talk to? ###

* For any problem or enquiry please refer to [the contact section of my website](http://ldsoftware.it/contact)