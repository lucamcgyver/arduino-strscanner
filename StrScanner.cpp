/*
 * StrScanner.cpp
 *
 *  Created on: 27 ago 2015
 *      Author: luca
 *
 *  Copyright 2015 Luca Di Stefano
 *
 *  This file is part of StrScanner.
 *
 *  StrScanner is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  StrScanner is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with StrScanner.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "StrScanner.h"
#include <Arduino.h>

StrScanner::StrScanner(String string) :
		_string(string), _delim(""), _next("") {
	processToken();
}

StrScanner::StrScanner(String string, String delimiter) :
		_string(string), _delim(delimiter), _next("") {
	processToken();
}

void StrScanner::useDelimiter(String delimiter) {
	_delim = delimiter;
}

bool StrScanner::hasNext() {
	return _next != "";
}

bool StrScanner::hasNextBool() {
	String tmp = _next;
	tmp.toLowerCase();
	return (tmp == "0" || tmp == "1" || tmp == "true" || tmp == "false"
			|| tmp == "yes" || tmp == "no" || tmp == "t" || tmp == "f" || tmp == "y" || tmp == "n");
}

bool StrScanner::hasNextByte() {
	if (hasNextInt()) {
		if (_next.toInt() < 255 && _next.toInt() > 0) {
			return true;
		}
	}
	return false;
}

bool StrScanner::hasNextChar() {
	return _next.length() == 1;
}

bool StrScanner::hasNextDouble() {
	for (int i = 0; i < _next.length(); i++) {
		if (!isdigit(_next.charAt(i)))
			if (_next.charAt(i) != '.')
				return false;
	}
	return true;
}

bool StrScanner::hasNextInt() {
	for (int i = 0; i < _next.length(); i++) {
		if (!isdigit(_next.charAt(i)))
			return false;
	}
	return true;
}

String StrScanner::next() {
	return processToken();
}

byte StrScanner::nextByte() {
	return next().toInt();
}

bool StrScanner::nextBool() {
	String tmp = processToken();
	tmp.toLowerCase();
	if (tmp == "false" || tmp == "0" || tmp == "no" || tmp == "f" || tmp == "n")
		return false;
	if (tmp == "true" || tmp == "1" || tmp == "yes" || tmp == "t" || tmp == "y")
		return true;

	return false;
}

char StrScanner::nextChar() {
	return next().charAt(0);
}

double StrScanner::nextDouble() {
	return next().toFloat();
}

int StrScanner::nextInt() {
	return next().toInt();
}

/*
 * PRIVATE METHODS
 */
String StrScanner::processToken() {
	String old = _next;
	String candidate;
	if (_string.charAt(0) == '\0') {
		_next = "";
	} else {
		if (_delim != "") {
			int index = _string.indexOf(_delim);
			candidate = _string.substring(0, index);
			_string = _string.substring(index + _delim.length());
		} else {
			candidate = String(_string.charAt(0));
			_string = _string.substring(1);
		}
		_next = candidate;
	}

	return old;
}
